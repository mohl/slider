<?php

/**
 * Slider_Admin
 * Class is used to control admin specific logic
 */

class Slider_Admin
{

	/**
	 * Render slider form
	 * 
	 * @return void
	 * @access public
	 */
	public function index($data)
	{
		$data = [
			'slider_height' => get_post_meta($data->ID, 'slider_height', true),
			'slider_width'   => get_post_meta($data->ID, 'slider_width', true),
			'slider_duration'   => get_post_meta($data->ID, 'slider_duration', true)
		];

		require_once SLIDER_ADMIN . 'views/form.php';
	}

	/**
	 * Store custom metabox values
	 * 
	 * @param object $post_id
	 * @return void
	 * @access private
	 */
	public function store($post_id)
	{
		update_post_meta(
			$post_id,
			'slider_height',
			FILTER_VAR($_POST['slider_height'], FILTER_SANITIZE_STRING)
		);

		update_post_meta(
			$post_id,
			'slider_width',
			FILTER_VAR($_POST['slider_width'], FILTER_SANITIZE_STRING)
		);

		update_post_meta(
			$post_id,
			'slider_duration',
			FILTER_VAR($_POST['slider_duration'], FILTER_SANITIZE_STRING)
		);
	}

	/**
	 * Add metabox
	 * 
	 * @return void
	 * @access public
	 */
	public function metaboxes()
	{
		add_meta_box(
			'slide_data',
			'Slide Data',
			[$this, 'index'],
			'slider',
			'side',
			'high'
		);	
	}

}
