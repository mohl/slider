<div class="wrap">	
	<div class="field">
		<label>
			Height: <br>
			<input type="height" class="large-text" name="slider_height" value="<?php echo $data['slider_height']; ?>">
		</label>
	</div>
	<div class="field">
		<label>
			Width: <br>
			<input type="text" class="large-text" name="slider_width" value="<?php echo $data['slider_width']; ?>">
		</label>
	</div>
	<div class="field">
		<label>
			Duration between slides: <br>
			<input type="text" class="large-text" name="slider_duration" value="<?php echo $data['slider_duration']; ?>">
		</label>
	</div>
</div>

