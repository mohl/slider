<div>
	<p>Choose a slider</p>

	<p>
		<?php print_r($instance); ?>
	</p>
	
	<?php if ( have_posts() ): ?>
	<select name="<?php echo $this->get_field_name('slider'); ?>">
	
	<?php while ( have_posts() ) : the_post(); ?>
		<option 
			value="<?php echo get_the_ID(); ?>"
			<?php echo $instance['slider'] == get_the_ID() ? 'selected' : ''; ?>
			>
			<?php echo the_title() ?>
		</option>
	<?php wp_reset_postdata(); endwhile; ?>
	
	</select>
	<?php else: ?>
	<p>
		No sliders found!
	</p>
	<?php endif; ?>
</div>
