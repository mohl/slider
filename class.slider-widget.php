<?php

class Slider_Widget extends WP_Widget
{

	public function __construct() {
		parent::__construct('slider-widget', 'Slider widget', [
			'description' => 'Simple slider widget',
			'classname' => 'slider-widget'
		]);
	}

	public function widget($args, $instance)
	{
		$slider_id = $instance['slider'];

		$args = [
			'p'          => FILTER_VAR($slider_id, FILTER_SANITIZE_NUMBER_INT),
			'post_type'   => 'slider',
			'post_status' => 'publish',
		];

		$query = new WP_Query($args);
		if ( $query->have_posts() ) {
			while( $query->have_posts() ) {
				$query->the_post();

				$slider_data = [
					'height'   => get_post_meta(get_the_ID(), 'slider_height', true),
					'width'    => get_post_meta(get_the_ID(), 'slider_width', true),
					'duration' => get_post_meta(get_the_ID(), 'slider_duration', true)
				];
				
				require_once SLIDER_CLIENT . 'views/slider.php';
			}
		}
		wp_reset_postdata();
	}

	public function form($instance)
	{

		$slider = $instance['slider'];
		$args = [
			'post_type' => 'slider',
			'post_status' => 'publish',
		];

		query_posts($args);

		require SLIDER_ADMIN . 'views/widget.php';

		wp_reset_query();

	}

	public function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		$instance['slider'] = FILTER_VAR($new_instance['slider'], FILTER_SANITIZE_STRING);
        return $instance;
	}

}
