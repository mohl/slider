<?php if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * @package Slider
 * 
 * Plugin Name: Slider
 * Description: Just a simple slider
 * Author: Morten Hartvig Larsen <mohl@protonmail.com>
 * Version: 0.0.1
 */

define( 'SLIDER_DIR' , plugin_dir_path( __FILE__ ) );
define( 'SLIDER_ADMIN', SLIDER_DIR . 'admin/' );
define( 'SLIDER_CLIENT', SLIDER_DIR . 'client/' );

remove_filter( 'the_content', 'wpautop' );

add_action('widgets_init', function() {
	require_once 'class.slider-widget.php';
	register_widget('Slider_Widget');
});

add_action( 'init' , function () {
	require 'class.slider.php';
	new Slider;
});
