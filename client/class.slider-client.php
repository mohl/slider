<?php

/**
 * Slider_Client
 * Class is used to control client specific logic
 */

class Slider_Client
{

	/**
	 * Shortcode 
	 * 
	 * @param array $argv List of arguments
	 * @return void
	 * @access public
	 */
	public function shortcode($argv)
	{
		if ( !$argv ) {
			echo 'Please provide a slider name.';
			return;
		}

		$this->slider($argv[0]);
	}

	/**
	 * Render the slider
	 * 
	 * @param string $slider Title of the slider
	 * @return void
	 * @access private
	 */
	private function slider($slider)
	{

		$args = [
			'title'          => FILTER_VAR($slider, FILTER_SANITIZE_STRING),
			'post_type'      => 'slider',
			'post_status'    => 'publish',
			'post_mine_type' => 'image'
		];

		$query = new WP_Query($args);
		if ( $query->have_posts() ) {
			while( $query->have_posts() ) {
				$query->the_post();

				$id = get_the_ID();

				$slider_data = [
					'height'   => get_post_meta($id, 'slider_height', true),
					'width'    => get_post_meta($id, 'slider_width', true),
					'duration' => get_post_meta($id, 'slider_duration', true)
				];

				require_once SLIDER_CLIENT . 'views/slider.php';
			}
		}
		wp_reset_postdata();

	}

	/**
	 * Enqueue scripts and CSS
	 * 
	 * @return void
	 * @access public
	 */
	public function scripts()
	{
		wp_enqueue_style(
			'plugin.slides.css',
			plugin_dir_url( __FILE__) . 'assets/css/plugin.slides.css',
			'',
			NULL,
			'all'
		);

		wp_enqueue_script(
			'jquery.slides.min.js',
			plugin_dir_url( __FILE__) . 'assets/js/jquery.slides.min.js',
			['jquery'],
			NULL,
			TRUE
		);

		wp_enqueue_script(
			'plugin.slider.js',
			plugin_dir_url( __FILE__) . 'assets/js/plugin.slider.js',
			['jquery', 'jquery.slides.min.js'],
			NULL,
			TRUE
		);
	}

}
