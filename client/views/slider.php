<div id="slides" 
	data-height="<?php echo $slider_data['height']; ?>" 
	data-width="<?php echo $slider_data['width']; ?>" 
	data-duration="<?php echo $slider_data['duration']; ?>">
	<?php the_content(); ?>
</div>
