const slide = jQuery('#slides');

if ( slide ) {

	slide.slidesjs({
		play: {
			interval: slide.data('duration') ? slide.data('duration') : 2000,
			auto: true
		}
	});

	jQuery('.slidesjs-container').css({
		height: slide.data('height') ? slide.data('height') : '',
		width: slide.data('width') ? slide.data('width') : '100%'
	});

}
