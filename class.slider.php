<?php if ( ! defined( 'ABSPATH' ) ) exit;

class Slider
{

	/**
	 * Create a new Slider instance
	 * 
	 * @return void
	 * @access public
	 */
	public function __construct()
	{
		$this->load_dependencies();
		$this->define_post_types();
		$this->define_client_hooks();
		$this->define_admin_hooks(); 
	}

	/**
	 * Load the required files for the plugin
	 * 
	 * @return void
	 * @access private
	 */
	private function load_dependencies()
	{
		require_once SLIDER_ADMIN  . 'class.slider-admin.php';
		require_once SLIDER_CLIENT . 'class.slider-client.php';
	}

	/**
	 * Define the posttypes needed for the slider plugin
	 * 
	 * @return void
	 * @access public
	 */
	public function define_post_types()
	{
		register_post_type( 'slider',
			[
				'labels' => [
					'name' => __( 'Slider' ),
					'singular_name' => __( 'Slider' )
				],
				'public' => true,
				'publicly_queryable' => false,
				'supports' => [
					'title',
					'editor'
				]
			]
		);
	}

	/**
	 * Define hooks used by the client
	 * 
	 * @return void
	 * @access private
	 */
	private function define_client_hooks()
	{
		$client = new Slider_Client;
		add_shortcode('slider', [$client, 'shortcode']);
		add_action('wp_enqueue_scripts', [$client, 'scripts']);
	}

	/**
	 * Define hooks used by the admin
	 * 
	 * @return void
	 * @access private
	 */
	private function define_admin_hooks()
	{
		$admin = new Slider_Admin;
		add_action('add_meta_boxes', [$admin, 'metaboxes']);
		add_action('save_post', [$admin, 'store']);
	}

}
